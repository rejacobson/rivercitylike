Creature = function() {
  Actions.call(this);
}

Actions = function() {
  var player = this;
  
  this.move_north = function(){
    player.z -= 1;
  };
  
  this.move_south = function(){
    player.z += 1;
  };
  
  this.move_west = function(){
    player.facing('west');
    player.x -= player.speed;
  };
  
  this.move_east = function(){
    player.facing('east');
    player.x += player.speed;
  };
  
  this.attack = function(){
    player.animation('attacking');
  };
  
  this.jump = function(){
    player.animation('jumping');
  };
  
  this.run = function() {
    player.animation('running');
    player.speed = player.base.speed * 2;
  };
  
  this.walk = function() {
    player.animation('walking');
    player.speed = player.base.speed;
  };
  
  this.idle = function() {
    player.animation('idle');
  };
}

KeyActionMap = function() {
  this.map = {
         'K_w_hold' : ['move_north'],
         'K_s_hold' : ['move_south'],
         'K_a_hold' : ['move_west'],
         'K_d_hold' : ['move_east'],
       'K_d_double' : ['run', 'move_east'],
       'K_a_double' : ['run', 'move_west'],
          'K_SPACE' : ['jump'],
           'K_CTRL' : ['attack']
  };
  
  

}


InputManager = function() {
  // Actions are keypresses that only occur once
  var actions = {};
  
  // States are keypresses that occur while the key is still down.
  var states = {};
    
  // Controllers map actions (entity method names), to key codes
  var controllers = [];

  var time;
  
  this.register = function(input_controller) {
    controllers.push(input_controller);
  }

  this.handleEvent = function(event) {
    // Clear the single event, keydown queue
    actions = {};
    
    // Get the time this press happened
    time = (new Date).getTime();
    
    // Keyup
    if (event.type === gamejs.event.KEY_UP) {
      delete states[event.key];
    }   
  
    // Keydown
    if (event.type === gamejs.event.KEY_DOWN) {
      actions[event.key] = time;
      states[event.key] = true;
    }
  }
  
  this.update = function(msDuration) {
    for (key in this.actions) {
      _.each(this.controllers, function(c) {
        if (!c.implements(key)) continue;
        c.execute(key);
      }
    }
    
    for (key in this.states) {
      key = key+'_hold';
      
      _.each(this.controllers, function(c) {
        if (!c.implements(key)) continue;
        c.execute(key);
      }
    }
  }
}



 __________ 
| Keyboard |
|__________|
      |     
 _____|____     __________ 
|ActionMap |   |   AI     |
|__________|   |__________|
      |              |
 _____|____     _____|____
| Actions  |   | Actions  |
|__________|   |__________|
      |              |
 _____|____     _____|____
|Character |   |Character |
|__________|   |__________|


Keyboard:
 Input K_a -> move_west
 Input K_s -> move_south
 
AI
 calculate -> move_north
 calculate -> move_south


Actions {
  move_north: function(),
  move_south: function(),
  ...
}

InputController
AIController

InputController = function(entity, actionMap) {
  this.entity = entity;
  this.actionMap = actionMap;
  
  this.implements = function(key) {
    return !!this.actionMap[key];
  };
  
  this.execute = function(key) {
    _.each(this.actionMap[key], function(func) {
      if (this.entity[func]) {
        this.entity[func].call(this.entity);
      }
    };
  };
}

AIController = function(entity) {
  this.entity = entity;
  
  this.getAIActions = function() {
  
  }

  this.execute = function(action) {
    this.entity[action].call(this.entity);
  }
}


Scene = function() {
  this.player = new Player();
  this.monster = new Monster();
  
  this.keyboard = new InputManager();
  
  this.player.controller = new InputController(this.player, player_action_map);
  this.monster.controller = new AIController(this.monster, behavior_model);
  
  this.keyboard.register(this.player.controller);
  
  this.handleEvent = this.keyboard.handleEvent;
}
