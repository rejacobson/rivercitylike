var gamejs = require('gamejs');
var event = gamejs.event;

var INACTIVE = exports.INACTIVE = 0;
var IDLE = exports.IDLE = 1;
var WALIKING = exports.WALKING = 2;
var RUNNING = exports.RUNNING = 3;

var RIGHT = exports.RIGHT = 1;
var LEFT = exports.LEFT = -1;

var X = exports.X = 0;
var Y = exports.Y = 1;
var Z = exports.Z = 2;

var Shadow = (function(){
  var instance;

  function shadow_instance() {
    this.image = gamejs.image.load('images/shadow.png');
    this.rect = new gamejs.Rect([0, 0], this.image.getSize());

    var offset = [this.rect.width / 2, this.rect.height / 2];

    this.draw = function(display, x, z) {
      this.rect.topleft = [x - offset[0], z - offset[1]];
      display.blit(this.image, this.rect);
    }
  }

  return {
    getInstance: function() {
      if ( !instance ) {
        instance = new shadow_instance();
      }

      return instance;
    },

    draw: function(display, x, z) {
      this.getInstance().draw(display, x, z);
    }
  };

}());


//////////////////////////////////////////////////////////
// Stats
//////////////////////////////////////////////////////////
var Stats = exports.Stats = function(stats) {
  var _stats = ['hp', 'strength', 'speed', 'weapon'];
  var assigns = {};
 
  _.each(_stats, function(stat) {
    assigns[stat] = stats[stat] || null;
  });

  this.base_stats = assigns;
  this.stats = _.clone(assigns);
}

//////////////////////////////////////////////////////////
// Creature
//////////////////////////////////////////////////////////
var Creature = exports.Creature = function(name, settings) {
  Stats.call(this, settings['stats']);
 
  this.name = name; 

  this.velocity = [0, 0, 0];
  this.x = settings.x || 0;
  this.y = settings.y || 0;
  this.z = settings.z || 0;

  this.facing = RIGHT;  // Facing positive direction; east
  this.on_ground = true;

  this.avatar = settings.avatar;

  this.update_callback = settings.update || null;
};

Creature.prototype.update = function(msDuration) {
  if (this.avatar.update) this.avatar.update(msDuration);

  this.x += this.velocity[X] * msDuration;
  this.y += this.velocity[Y] * msDuration;
  this.z += this.velocity[Z] * msDuration;

  // Gravity
  if (this.y > 0) this.velocity[Y] -= 2000 * msDuration;

  // Friction
  if (this.on_ground) {
    if (this.velocity[X] != 0 ) {
      this.velocity[X] -= (this.velocity[X]*8 * msDuration);

      if (Math.abs(this.velocity[X]) < 10) {
        this.velocity[X] = 0;
      }
    }

    if (this.velocity[Z] != 0 ) {
      this.velocity[Z] -= (this.velocity[Z]*10 * msDuration);

      if (Math.abs(this.velocity[Z]) < 10) {
        this.velocity[Z] = 0;
      }
    }

    if (this.velocity[X] == 0 && this.velocity[Z] == 0 && this.avatar.state() != 'idle') {
      this.avatar.state('idle'); 
    }
  }

  // Don't fall through the ground
  if (this.y < 0 ) {
    this.on_ground = true;
    this.y = 0;
    this.velocity[Y] = 0;
  }

  if (this.update_callback) {
    this.update_callback.call(this, msDuration);
  }
};

Creature.prototype.draw = function(display) {
  Shadow.draw(display, this.x, this.z);
  if (this.avatar.draw) this.avatar.draw(display, this.x, this.z - this.y);
}

Creature.prototype.face = function(direction) {
  this.facing = direction;
}

var basic_map = {};
basic_map[event.K_w+'_hold'] = ['move_north'];
basic_map[event.K_s+'_hold'] = ['move_south'];
basic_map[event.K_a+'_hold'] = ['walk', 'move_west'];
basic_map[event.K_d+'_hold'] = ['walk', 'move_east'];
basic_map[event.K_d+'_dbl_hold'] = ['run', 'move_east'];
basic_map[event.K_a+'_dbl_hold'] = ['run', 'move_west'];
basic_map[event.K_SPACE] = ['jump'];
basic_map[event.K_b] = ['attack'];
exports.basic_action_map = basic_map;

exports.BasicActions = {
  walk_action: function(msDuration) {
    this.stats.speed = this.base_stats.speed;
    this.avatar.state('walk');
  },

  run_action: function(msDuration) {
    this.stats.speed = this.base_stats.speed * 3;
    this.avatar.state('walk');
  },

  move_north_action: function(msDuration){
    if (this.on_ground) this.velocity[Z] = -this.base_stats.speed * 0.8;
    this.avatar.state('walk');
  },
  
  move_south_action: function(msDuration){
    if (this.on_ground) this.velocity[Z] = this.base_stats.speed * 0.8;
    this.avatar.state('walk');
  },
  
  move_west_action: function(msDuration){
    if (!this.on_ground) return this.face(LEFT);

    if (this.velocity[X] <= 0) {
      this.face(LEFT);
      this.velocity[X] = -this.stats.speed;
    }
  },
  
  move_east_action: function(msDuration){
    if (!this.on_ground) return this.face(RIGHT);

    if (this.velocity[X] >= 0) {
      this.face(RIGHT);
      this.velocity[X] = this.stats.speed;
    }
  },

  attack_action: function(msDuration){
    this.velocity[X] = 350 * this.facing;
    //player.animation('attacking');
  },
  
  jump_action: function(msDuration){
    if (this.on_ground) {
      this.on_ground = false;
      this.velocity[Y] = 450;
      //player.animation('jumping');
    }
  },
  
  idle_action: function(msDuration) {
    //player.animation('idle');
  }
}

gamejs.preload(['images/shadow.png']);
