var gamejs = require('gamejs');
var entity = require('entity');
var input = require('input');
var avatar = require('avatar');

function entity_sort(a, b) {
  return a.z - b.z;
}

var GameScene = exports.GameScene = function(game) {
  this.game = game;
  this.input_router = new input.Router();

  this.entities = [
    new entity.Creature('npc1', {
      stats: {},
      avatar: new avatar.Image('images/player2.png'),
      x: 25,
      z: 35
    }),

    new entity.Creature('npc2', {
      stats: {},
      avatar: new avatar.Image('images/player3.png'),
      x: 15,
      z: 100
    })
  ]; 

  
  this.player = new entity.Creature('player', {
    stats: {
      hp: 15,
      strength: 12,
      speed: 100
    },
    avatar: avatar.Animation.factory(avatar.animations.player),
    x: 50,
    z: 50,
    update: function(msDuration) { }
  });

  this.entities.push(this.player);

  this.player.controller = new input.Controller(this.player, entity.basic_action_map, entity.BasicActions);
  this.input_router.register(this.player.controller);

  this.handleEvent = this.input_router.handleEvent;

  /////////////////////////
  // Update
  /////////////////////////
  this.update = function(msDuration) {
    this.input_router.update(msDuration);

    _.each(this.entities, function(e) {
      e.update(msDuration);
    });

    this.entities.sort(entity_sort);
  }
  
  /////////////////////////
  // Draw
  /////////////////////////
  this.draw = function(display) {
    // Clear the canvas before drawing
    display.clear();

    _.each(this.entities, function(e) {
      e.draw(display);
    });
  }
  
  this.destroy = function() {}
};


gamejs.preload(['images/player.png', 'images/player2.png', 'images/player3.png']);
